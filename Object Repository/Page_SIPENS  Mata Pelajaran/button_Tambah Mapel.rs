<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Tambah Mapel</name>
   <tag></tag>
   <elementGuidId>9c22026c-8a32-441b-b01a-c3810f29d3e3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-primary</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@type='button']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>49509c8f-94bb-4fd9-939b-7cb99a697d86</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>5f53d50a-23cd-421f-8999-ba6896a7554a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary</value>
      <webElementGuid>fc15ba81-5289-4fb4-acc7-139ba01d1f85</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>modal</value>
      <webElementGuid>640f9acf-46d6-4ffe-a740-24ce365d75d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-target</name>
      <type>Main</type>
      <value>#inputData</value>
      <webElementGuid>ce2827db-efdc-43ff-8fc2-9cd3ca863e4b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                        Tambah Mapel
                    </value>
      <webElementGuid>2544ebe3-8006-4e5c-b635-c57dc63b8fff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[2]/div[@class=&quot;card col-lg-12 col-md-12&quot;]/div[@class=&quot;card-body p-0 pt-2 pb-4&quot;]/button[@class=&quot;btn btn-primary&quot;]</value>
      <webElementGuid>ad9cdd77-8e70-427e-8cb7-02d75c92b483</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@type='button']</value>
      <webElementGuid>656ad785-b84e-481e-ac9f-a3eeb3e2893f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section[2]/div/div[2]/button</value>
      <webElementGuid>eb131fa3-1b20-48dd-93ab-b335ee61cbef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Delete'])[1]/following::button[1]</value>
      <webElementGuid>bc9d333e-8f28-4e12-aad4-311e535eaee1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Edit'])[1]/following::button[1]</value>
      <webElementGuid>34dcb74e-08c2-45ce-a71e-a272ece2585e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mata Pelajaran'])[3]/preceding::button[1]</value>
      <webElementGuid>967b290a-b56f-478b-9461-38fbee5352de</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button</value>
      <webElementGuid>a7afb153-33eb-4d02-a6fb-b3c3e724a458</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = '
                        
                        Tambah Mapel
                    ' or . = '
                        
                        Tambah Mapel
                    ')]</value>
      <webElementGuid>620e05bf-42c6-436b-8b67-20f74be164e1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
