<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Import Data</name>
   <tag></tag>
   <elementGuidId>0508f2fd-2438-48c1-94fa-181ca7db2611</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#modalButton</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='modalButton']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>121576bd-d9fe-425a-8755-4f5acd1142c6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>130afeb3-4057-4df2-b1b5-8900eea4295d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary</value>
      <webElementGuid>bcda00e2-c4d8-4957-b0f3-8bc29943054a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>modalButton</value>
      <webElementGuid>1e29fa8c-1293-48f8-ab1c-efe7bbdcd80a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                        Import Data
                    </value>
      <webElementGuid>89c13256-cd47-4ec2-a29d-5888aac270f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;modalButton&quot;)</value>
      <webElementGuid>8a596b09-f0eb-4cc9-b011-355b985e2237</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='modalButton']</value>
      <webElementGuid>1e7f76a2-2035-46ea-8e92-655eca2691f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section[2]/div/div[2]/button[2]</value>
      <webElementGuid>ef0b39d6-acea-4e06-9e82-611500c64809</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tambah Data Guru'])[1]/following::button[2]</value>
      <webElementGuid>90409044-80a7-4cc3-af50-15c8de2bee66</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Daftar Data Guru'])[1]/preceding::button[1]</value>
      <webElementGuid>90d90f3a-e47c-4e3a-b93e-004b986f1649</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Import Data']/parent::*</value>
      <webElementGuid>64b50880-d1d3-4df3-a470-5884acce4ca4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button[2]</value>
      <webElementGuid>b27a998a-adbf-414b-bbd3-21a9f4ceb0e8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and @id = 'modalButton' and (text() = '
                        
                        Import Data
                    ' or . = '
                        
                        Import Data
                    ')]</value>
      <webElementGuid>907cee0e-682d-463a-8620-bc758028c4de</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
