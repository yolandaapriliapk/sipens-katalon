<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Tambah Data Guru</name>
   <tag></tag>
   <elementGuidId>14726115-2900-4432-a9cf-fb3d9f8329d2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>button.btn.btn-success</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@type='button']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>9c6fce42-8eec-4f23-9f41-163ef8426de8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>6c4aa285-a822-4489-9ed3-a51e565843ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-success</value>
      <webElementGuid>88e0e9b1-7d98-45ca-8284-682c6c6c21a7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>modal</value>
      <webElementGuid>50f70366-7f87-45ec-8e8a-05d6844f8077</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-target</name>
      <type>Main</type>
      <value>#inputData</value>
      <webElementGuid>d67e17c9-7f61-414f-9858-c83ff2d4dfb9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                        Tambah Data Guru
                    </value>
      <webElementGuid>46925700-a007-42ec-b4ee-455004e04f21</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[2]/div[@class=&quot;card col-lg-12 col-md-12&quot;]/div[@class=&quot;card-body p-0 pt-2 pb-4&quot;]/button[@class=&quot;btn btn-success&quot;]</value>
      <webElementGuid>d231af8d-83cd-431f-9b2a-8cb9ab1378cc</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@type='button']</value>
      <webElementGuid>d99e6f19-d4a2-4e20-b939-fd22a29690b2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section[2]/div/div[2]/button</value>
      <webElementGuid>ff3abf36-6cdf-43ec-a489-38e6684b605a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tambah Data Guru'])[1]/following::button[1]</value>
      <webElementGuid>81363b97-389e-435b-85f4-30eb402a90ce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Informasi Pengguna'])[1]/following::button[1]</value>
      <webElementGuid>54a472ba-1184-40c0-abec-ce2912b4214a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//button</value>
      <webElementGuid>8ab0ae7c-21c6-4b94-a5c0-29adbf310116</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and (text() = '
                        
                        Tambah Data Guru
                    ' or . = '
                        
                        Tambah Data Guru
                    ')]</value>
      <webElementGuid>4c0a1c18-ac2f-4519-807c-51238ae8cc09</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
