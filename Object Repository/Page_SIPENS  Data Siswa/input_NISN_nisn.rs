<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_NISN_nisn</name>
   <tag></tag>
   <elementGuidId>28c1b294-c259-4b89-a01f-62bf1f7c03ca</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@name='nisn']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input[name=&quot;nisn&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>e7fa834d-6a72-40ae-ae06-682f0e27ad2e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>dcc2908a-c9d4-43c1-86f5-fc9ff8222cae</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>5a61f1db-c446-43e9-9ca0-bf74754d929c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>nisn</value>
      <webElementGuid>282ab7b1-9353-4322-a022-173be08aab0c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>0093547383</value>
      <webElementGuid>57dfe085-868a-49a7-b379-c70a8aed40d9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>10</value>
      <webElementGuid>0e7f3944-55c0-4286-b867-b00801b4099f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[3]/form[@class=&quot;needs-validation&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]/div[@class=&quot;col-md-12 col-lg-12 p-0&quot;]/div[@class=&quot;form-group row&quot;]/div[@class=&quot;col-md-3 col-lg-3 mb-4&quot;]/input[@class=&quot;form-control&quot;]</value>
      <webElementGuid>4e8c4df0-f370-445e-b4e0-5cc0b500d4df</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@name='nisn']</value>
      <webElementGuid>6a510d6f-9a8f-4a73-ad03-87fdf8e72262</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section[3]/form/div/div[2]/div/div/div/input</value>
      <webElementGuid>0edbc274-db95-49c6-89cf-ded7c5d2cf23</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/input</value>
      <webElementGuid>ea1b6605-ed14-4a7a-a501-2ad9fb1516d7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @name = 'nisn']</value>
      <webElementGuid>295963db-159b-4557-ac30-73e29cf524d2</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
