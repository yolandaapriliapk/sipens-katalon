<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Lihat Siswa</name>
   <tag></tag>
   <elementGuidId>37e7ed85-7641-448d-bbd1-d495f2fff7d7</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//table[@id='table-2']/tbody/tr/td[5]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>f0f98897-8837-420b-8bc0-b46f7c588f0b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>https://sipens.com/admin/show-plotting/tahun=7/kelas=7/subkelas=9</value>
      <webElementGuid>a4c45236-8f8d-47c1-ac21-69459245a599</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary</value>
      <webElementGuid>9fb79846-3705-4c41-8012-78aac98fde61</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                                
                                                Lihat Siswa
                                            </value>
      <webElementGuid>f87fc6f8-2744-4b8c-a455-bd3eadd0994c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;table-2&quot;)/tbody[1]/tr[1]/td[5]/a[@class=&quot;btn btn-primary&quot;]</value>
      <webElementGuid>0cc9f12e-99db-4772-a59f-40cf1ad1c67b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//table[@id='table-2']/tbody/tr/td[5]/a</value>
      <webElementGuid>d407ff4c-bcbd-4858-ba0f-c623ecbfd9a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='A'])[1]/following::a[1]</value>
      <webElementGuid>6ae19363-6f2d-47b3-acbc-872635b3f5f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Action'])[1]/following::a[1]</value>
      <webElementGuid>00a6ea91-8b02-4714-bed5-19f6a38d395d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='B'])[1]/preceding::a[1]</value>
      <webElementGuid>ce8a13a0-b213-4c25-ae25-154f9c9400f1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Lihat Siswa']/parent::*</value>
      <webElementGuid>91164607-bcec-4a57-9e45-3d106a8a2e80</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, 'https://sipens.com/admin/show-plotting/tahun=7/kelas=7/subkelas=9')]</value>
      <webElementGuid>a62204ff-6843-4b78-b810-f73c1712c9a0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//td[5]/a</value>
      <webElementGuid>5526dda3-b68c-4b4f-bff9-fb99e0525f57</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'https://sipens.com/admin/show-plotting/tahun=7/kelas=7/subkelas=9' and (text() = '
                                                
                                                Lihat Siswa
                                            ' or . = '
                                                
                                                Lihat Siswa
                                            ')]</value>
      <webElementGuid>48954929-80e5-47d2-b65c-e10b1c55a379</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
