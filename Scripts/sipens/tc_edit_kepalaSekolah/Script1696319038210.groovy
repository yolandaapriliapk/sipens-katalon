import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://sipens.com/login')

WebUI.setText(findTestObject('Object Repository/Page_Login  SIPENS/input_Username_username'), '20500492')

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Login  SIPENS/input_Forgot Password_password'), 'LjW/QG7ZUgIynvlB3Cr/Vw==')

WebUI.click(findTestObject('Object Repository/Page_Login  SIPENS/button_Login'))

WebUI.click(findTestObject('Object Repository/Page_SIPENS  Dashboard/button_OK'))

WebUI.click(findTestObject('Object Repository/Page_SIPENS  Dashboard/span_Kepala Sekolah'))

WebUI.click(findTestObject('Object Repository/Page_SIPENS  Kepala Sekolah/a_Edit'))

WebUI.click(findTestObject('Object Repository/Page_SIPENS  Edit Kepala Sekolah/button_Ubah Data'))

WebUI.setText(findTestObject('Object Repository/Page_SIPENS  Edit Kepala Sekolah/input_Gelar Depan_gelar_depan'), 'Dr')

WebUI.setText(findTestObject('Object Repository/Page_SIPENS  Edit Kepala Sekolah/input_NIP_nip'), '1234567890')

WebUI.setText(findTestObject('Object Repository/Page_SIPENS  Edit Kepala Sekolah/input_NIK_nik'), '1234567887654321')

WebUI.click(findTestObject('Object Repository/Page_SIPENS  Edit Kepala Sekolah/input_Tempat Lahir_tmp_lahir'))

WebUI.setText(findTestObject('Object Repository/Page_SIPENS  Edit Kepala Sekolah/input_Email_email'), 'test3@gmail.com')

WebUI.setText(findTestObject('Object Repository/Page_SIPENS  Edit Kepala Sekolah/input_Tempat Lahir_tmp_lahir'), 'Malang')

WebUI.setText(findTestObject('Object Repository/Page_SIPENS  Edit Kepala Sekolah/input_Alamat_alamat'), 'Jl Bahagia')

WebUI.setText(findTestObject('Object Repository/Page_SIPENS  Edit Kepala Sekolah/input_No. HP_no_hp'), '081234567890')

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_SIPENS  Edit Kepala Sekolah/select_Islam                               _7c2cf0'), 
    'Islam', true)

WebUI.selectOptionByValue(findTestObject('Object Repository/Page_SIPENS  Edit Kepala Sekolah/select_Laki - Laki                         _9377a6'), 
    'L', true)

WebUI.setText(findTestObject('Object Repository/Page_SIPENS  Edit Kepala Sekolah/input_Pendidikan Terakhir_pendidikan'), 
    'S3')

WebUI.click(findTestObject('Object Repository/Page_SIPENS  Edit Kepala Sekolah/button_Ubah Data_1'))

WebUI.verifyElementText(findTestObject('Object Repository/Page_SIPENS  Edit Kepala Sekolah/div_Edit Data Kepala Sekolah Berhasil'), 
    'Edit Data Kepala Sekolah Berhasil')